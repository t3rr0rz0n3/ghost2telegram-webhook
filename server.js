const express = require("express");
const bodyParser = require("body-parser");
const basicAuth = require("express-basic-auth");
require("dotenv").config();
const axios = require("axios");
const TOKEN = process.env.TELEGRAM_BOT_TOKEN;
const channelID = process.env.TELEGRAM_CHANNEL_ID;
const telegram_url = `https://api.telegram.org/bot${TOKEN}/sendMessage?chat_id=${channelID}&text=`;

const app = express();
const PORT = process.env.PORT;
let lastRequest = 0;

app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));

app.use(
  basicAuth({
    users: {
      [process.env.AUTH_USERNAME]: process.env.AUTH_PASSWORD,
    },
  })
);

app.post("/publish", (req, res) => {
  if (Date.now() - 5000 > lastRequest) {
    handlePublish(req);
  }
  res.status(200).send("OK");
});

app.listen(PORT, '127.0.0.1', () => console.log(`🚀 Server running on port ${PORT}`));

const handlePublish = (req) => {
  const { url, feature_image, excerpt, title, tags } = req.body.post.current;
  let emoji = process.env.EMOJI;
  let primary_tag = tags[0].name;
  let message = '#' + primary_tag.toUpperCase() + '\n\n' + emoji + ' ' +`${title}\n\n` + `${excerpt}\n\n`;
  let content = message + `${url}\n`;
  sendToBot(content, feature_image);
};

const sendToBot = (content, feature_image) => {
  const contentEncoded = encodeURIComponent(content);
  const url_image = feature_image;
  const urlEncoded = telegram_url + contentEncoded + "&photo=" + url_image;
  axios
    .get(urlEncoded)
    .then((response) => {
      console.log(response);
    })
    .catch((err) => console.log(err));
};
