# Ghost2Telegram Webhook

This is ghost cms telegram channel webhook made with `express`

## How to use

+ Clone this repository
+ Run `npm install`
+ To test on local server run `npm run start`
+ Copy `.env-example` and rename to `.env`
+ Fill your data in `.env`
+ Access your webhook via https://{AUTH_USERNAME}:{AUTH_PASSWORD}@[YOUR_IP:PORT_OR_DOMAIN]/publish


